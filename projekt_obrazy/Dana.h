#ifndef Dana_h
#define Dana_h

//#pragma once
#include <cstdio>
#include <iostream> 
#include <cstdlib>  
#include <cmath>  
#include <ctime>
#include <vector>
#include <algorithm>
#include "ToFile.h"

#define M_PI 3.14159265358979323846

using namespace std;

// wywalic potem //
class KRN;
class KRR;


class Dana {
public:
	double x;
	int klasa;


	Dana(){};
	Dana(double x1, int k1);

	/*  nieuzywane
	Dana GenerujZRozkladuNormalnego(KRN klasa);
	Dana GenerujZRozkladuRownomiernego(KRR klasa);
	vector<Dana> GenerujCiagNormalny(KRN klasa1, int ilosc1, KRN klasa2, int ilosc2);
	vector<Dana> GenerujCiagRownomierny(KRR klasa1, int ilosc1, KRR klasa2, int ilosc2);
	*/
	static int	liczBledy(vector<Dana> ciagNowy, vector<Dana> klasy);
	static double czestoscBlednegoDopasowania(vector<Dana> ciagNowy, vector<Dana> klasy);
};

//Interfejs dla klas rozkladu
class IKlasaRozkladu{
public:

	virtual Dana GenerujZRozkladu() = 0;
	virtual vector<Dana> GenerujCiag(vector<Dana> &ciag, int ilosc2) = 0;
	virtual double prawdopodobieństwoWystapienia(double x) = 0;


};


//Klasa Rozkladu Normalnego
class KRN : public IKlasaRozkladu{
public:
	int klasa;
	double centrum;
	double sigma;

	KRN(int klasa, double centrum, double sigma) :
		klasa(klasa),
		centrum(centrum),
		sigma(sigma)
	{}

	// generuje dana z rozkladu normalnego
	Dana GenerujZRozkladu();
	// generuje ciag i zapisuje w vektorze z argumentu
	vector<Dana> GenerujCiag(vector<Dana> &ciag, int ilosc);
	// podaje prawdopodobienstwo wystapienia
	double prawdopodobieństwoWystapienia(double x);
	//Argumenty dla 'jak'
	//'P' - calka od nieskonczonosci do x
	//'K' - calka od x do nieskonczonosci
	double caukaRozkladu(char jak, double x);
	//cauka od wartosci do wartosci
	double caukaRozkladu(double odX, double doX);
};


//Klasa Rozkladu Rownowaznego
class KRR : public IKlasaRozkladu{
public:
	int klasa;
	double min;
	double max;

	KRR(int klasa, double min, double max) :
		klasa(klasa),
		min(min),
		max(max)
	{}

	// generuje dana z rozkladu rownomiernego
	Dana GenerujZRozkladu();
	// generuje ciag i zapisuje w wektorze z argumentu
	vector<Dana> GenerujCiag(vector<Dana> &ciag, int ilosc);
	// podaje prawdopodobienstwo wystapienia -- do napisania
	double prawdopodobieństwoWystapienia(double x);
};





#endif





