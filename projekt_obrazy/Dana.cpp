#include "stdafx.h"
#include "stdafx.h"
#include <cstdio>
#include <iostream> 
#include <cstdlib>  
#include <cmath>  
#include <ctime>
#include <vector>
#include <algorithm>
#include "Dana.h"
using namespace std;

// std - standard deviation == sigma - skala(odchylenie standardowe)
//mean - srednia
//rozklad normalny na podstawie Box-Muller transform



Dana KRN::GenerujZRozkladu(){
	double z1, u1, u2;

	u1 = rand() * (1.0 / RAND_MAX);
	u2 = rand() * (1.0 / RAND_MAX);

	z1 = sqrt(-2 * log(u1))*cos(2 * M_PI *u2);
	double wartosc = sigma*z1 + centrum;

	Dana dana(wartosc, klasa);

	return dana;
}


vector<Dana> KRN::GenerujCiag(vector<Dana> &ciag, int ilosc){

	for (int i = 0; i < ilosc; ++i)
	{
		Dana wygenerowana = GenerujZRozkladu();
		ciag.push_back(wygenerowana);
	}

	// uklada wektor losowo
	//	random_shuffle(ciag.begin(), ciag.end(), MojRandom);

	return ciag;
}

Dana KRR::GenerujZRozkladu(){
	double wartosc = min + (1.0 * rand() / RAND_MAX)*(max - min);

	Dana dana(wartosc, klasa);

	return dana;
}


vector<Dana> KRR::GenerujCiag(vector<Dana> &ciag, int ilosc){

	for (int i = 0; i < ilosc; ++i)
	{
		Dana wygenerowana = GenerujZRozkladu();
		ciag.push_back(wygenerowana);
	}

	// uklada wektor losowo
	//	random_shuffle(ciag.begin(), ciag.end(), MojRandom);

	return ciag;
}


Dana::Dana(double x1, int k1)
{
	x = x1;
	klasa = k1;

}


int	Dana::liczBledy(vector<Dana> ciagNowy, vector<Dana> klasy)
{
	int licznik = 0;
	for (int i = 0; i < ciagNowy.size(); i++)
	{
		if (ciagNowy[i].klasa != klasy[i].klasa)
			licznik++;
	}
	return licznik;
}

double Dana::czestoscBlednegoDopasowania(vector<Dana> ciagNowy, vector<Dana> klasy){
	double bledne = liczBledy(ciagNowy, klasy);
	return bledne / klasy.size();
}

//to sie nazywa gestosc prawdopodobienstwa
double KRN::prawdopodobieństwoWystapienia(double x){
	return (1 / (sqrt(2 * M_PI)*sigma)) * exp((-1)* pow((x - centrum), 2) / 2 * pow(sigma, 2));

}

double KRR::prawdopodobieństwoWystapienia(double x){

	if (x < min)
		return 0;
	else if (x < max)
		return 1 / (max - min);
	else if (x > max)
		return 0;
}


double KRN::caukaRozkladu(char jak, double x){

	double wartosc = 0;

	switch (jak){
	case 'P':
		wartosc = (1 + erf((x - centrum) / (sigma*sqrt(2)))) / 2;
		break;
	case 'K':
		wartosc = 1 - (1 + erf((x - centrum) / (sigma*sqrt(2)))) / 2;
		break;
	}

	return wartosc;
}


double KRN::caukaRozkladu(double odX, double doX){

	double odVar = (1 + erf((odX - centrum) / (sigma*sqrt(2)))) / 2;
	double doVar = (1 + erf((doX - centrum) / (sigma*sqrt(2)))) / 2;

	return doVar - odVar;

}