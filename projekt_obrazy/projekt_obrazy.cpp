// projekt_obrazy.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdio>
#include <iostream> 
#include <cstdlib>  
#include <cmath>  
#include <ctime>
#include <vector>
#include <algorithm>
#include "ToFile.h"
#include "Dana.h"
#include "alphaNN.h"
#include "NM.h"
#include "Bayes.h"



using namespace std;

//std - standard deviation == sigma - skala (odchylenie standardowe)
//mean - srednia
//rozklad normalny na podstawie Box-Muller transfor
//przeciazenie operatora << do wyswietlania zawartosci tab
ostream & operator << (ostream &wyjscie, Dana &dana){
	return wyjscie << "(" << dana.x << ", " << dana.klasa << ")";
}
void wyswietl(vector<Dana> tab)
{
	for (int k = 0; k < tab.size(); k++)
		cout << tab[k] << ", ";
	cout << endl;
}



int main(void)
{
	double srednia = 0;
	
	//for (int i = 1; i <= 10; i++)
	{
		enum Rodzaj{ NORMALNY, ROWNOMIERNY } rodzajBadania;

		// Zakomentuj odpowiedni

		rodzajBadania = NORMALNY;
		//rodzajBadania = ROWNOMIERNY;

		//-----------------------


		// Parametry rozkladu normalnego
		double sigma1 = 1;
		double sigma2 = 1;
		double centrum1 = 0;
		double centrum2 = 1;
		//-----------------------


		// Parametry do rozkladu rownomiernego
		double min1 = 0;
		double max1 = 1;
		double min2 = 2;
		double max2 = 3;
		//----------------------


		// Inne parametry
		double ilosc1 = 1000;
		double ilosc2 = 1000;
		double wSumie = ilosc1 + ilosc2;

		double iloscBadanego1 = 200;
		double iloscBadanego2 = 200;
		double wSumieBadanego = iloscBadanego1 + iloscBadanego2;
		//---------------


		// Przygotowanie klas rozklad�w
		KRN norm1(1, centrum1, sigma1);
		KRN norm2(2, centrum2, sigma2);

		KRR rown1(1, min1, max1);
		KRR rown2(2, min2, max2);
		//-----------------------


		// inne
		srand((unsigned int)time(NULL));
		double czestoscBledu;
		ToFile::zacznij();
		ToFile::wprowadzZmiennaDoPliku(wSumie, "iloscUczacego");
		//------

		// Polimorfizm dla ujednolicenia
		IKlasaRozkladu *klasaRozkladu1;
		IKlasaRozkladu *klasaRozkladu2;

		switch (rodzajBadania)
		{
		case NORMALNY:
			klasaRozkladu1 = &norm1;
			klasaRozkladu2 = &norm2;
			ToFile::wprowadzZmiennaDoPliku(norm1.centrum, "centrum1");
			ToFile::wprowadzZmiennaDoPliku(norm1.sigma, "sigma1");
			ToFile::wprowadzZmiennaDoPliku(norm2.centrum, "centrum2");
			ToFile::wprowadzZmiennaDoPliku(norm2.sigma, "sigma2");
			break;
		case ROWNOMIERNY:
			klasaRozkladu1 = &rown1;
			klasaRozkladu2 = &rown2;
			break;
		default:
			return -1;
		}
		//-------------------------





		// Generowanie ciagu uczacego
		vector<Dana> ciagUczacy;
		(*klasaRozkladu1).GenerujCiag(ciagUczacy, ilosc1);
		(*klasaRozkladu2).GenerujCiag(ciagUczacy, ilosc2);

		cout << "Dane z ciagu uczacego normalnego" << endl;
		//for (int i = 0; i < ciagUczacy.size(); ++i)
		//	cout << ciagUczacy[i] << "," << endl;
		ToFile::wprowadzWektorDoPliku(ciagUczacy, "ciagUczacy");

		//--------------------------


		// Generowanie ciagu badanego
		vector<Dana> ciagBadany;
		(*klasaRozkladu1).GenerujCiag(ciagBadany, iloscBadanego1);
		(*klasaRozkladu2).GenerujCiag(ciagBadany, iloscBadanego2);

		cout << endl << "Dane z ciagu badanego" << endl;
		//for (int i = 0; i < ciagBadany.size(); ++i)
		//	cout << ciagBadany[i] << "," << endl;

		vector<Dana> klasy = ciagBadany;
		//---------------------------


		//#####################
		// AlphaNN oraz NM
		//#####################



		// Badanie alphaNN
		vector<Dana> wynikAlphaNN = alphaNN::metodaAlphaNN(ciagUczacy, ciagBadany, 3);
		czestoscBledu = Dana::czestoscBlednegoDopasowania(wynikAlphaNN, klasy);
		cout << endl << "Metoda alphaNN:" << endl;
		//for (int i = 0; i < wynikAlphaNN.size(); ++i)
		//	cout << wynikAlphaNN[i] << "," << endl;
		cout << "Czestosc blednego dopasowania: " << czestoscBledu << endl;
		srednia += czestoscBledu;
		//----------------

		

		// Badanie NM
		vector<Dana>  wynikNN = NM::metodaNM(ciagUczacy, ciagBadany);
		czestoscBledu = Dana::czestoscBlednegoDopasowania(wynikNN, klasy);
		cout << endl << "Metoda sredniej:" << endl;
		//for (int i = 0; i < wynikNN.size(); ++i)
		//	cout << wynikNN[i] << "," << endl;
		cout << "Czestosc blednego dopasowania: " << czestoscBledu << endl;
		//-----------

		//#####################



		//########
		// Bayes
		//########
		vector<double> pXk1, pXk2;
		vector<Dana>  wynikBayes;

		// nie wiem o co chodzi
		pXk1 = Bayes::gestosc(ciagBadany, *klasaRozkladu1);//licze pstwa z ciagu uczacego - slabo ze tu, nie mam pomyslu 
		pXk2 = Bayes::gestosc(ciagBadany, *klasaRozkladu2);
		//-------

		wynikBayes = Bayes::metodaBayes(ciagBadany, pXk1, pXk2, Bayes::liczPrawdopodobienstwo(iloscBadanego1, wSumieBadanego), Bayes::liczPrawdopodobienstwo(ilosc2, wSumieBadanego));
		cout << "pstwo P1: " << Bayes::liczPrawdopodobienstwo(iloscBadanego1, wSumieBadanego) << ", pstwo P2: " << Bayes::liczPrawdopodobienstwo(iloscBadanego2, wSumieBadanego) << endl; //potrzebne do badan 

		cout << endl << "Metoda bayesa:" << endl;
		//for (int i = 0; i < wynikBayes.size(); ++i)
		//	cout << wynikBayes[i] << "," << endl;
		// brak ryzyka :( 

		//RYZYKO

		cout << ilosc1 << "::" << ilosc2 << endl;
		vector<double> granice = Bayes::liczGranice(klasaRozkladu1, klasaRozkladu2, iloscBadanego1 / wSumieBadanego, iloscBadanego2 / wSumieBadanego);

		cout << "ryzyko: " << Bayes::liczRyzyko(klasaRozkladu1, klasaRozkladu2, granice) << endl;
		double czestoscBayesa = Dana::czestoscBlednegoDopasowania(wynikBayes, ciagBadany);
		cout << "czestosc bledu: " << czestoscBayesa << endl;


		// ciag rozklad normalny 
		/*
		//KRR krr1(1, min2, max2), krr2(2, min1, max1);
		//ciagUczacy = dana.GenerujCiagRownomierny(krr1, ilosc1, krr2, ilosc2);

		cout << "Dane z ciagu rownomiernego" << endl;
		for (int i = 0; i < ciagUczacy.size(); ++i)
		cout << ciagUczacy[i] << "," << endl;

		ciagBadany = dana.GenerujCiagNormalny(klasaRozkladu1, ilosc1, klasaRozkladu2, ilosc2);

		klasy = ciagBadany;
		cout << endl;
		cout << "Dane z ciagu do oszacowania normalnego" << endl;
		for (int i = 0; i < ciagBadany.size(); ++i)
		cout << ciagBadany[i] << "," << endl;

		wynikAlphaNN=al.metodaAlphaNN(ciagUczacy, ciagBadany, 3);
		cout << "Metoda alphaNN:" << endl;
		for (int i = 0; i < wynikAlphaNN.size(); ++i)
		cout << wynikAlphaNN[i] << "," << endl;

		wynikNN = nm.metodaNM(ciagUczacy, ciagBadany);
		// czesctosBledu = (dana.liczBledy(wynikNN, klasy) / klasy.size());   //wyznaczanie czestosci bledu empirycznie - nie dziala jeszcze dla rownomiernego
		cout << "Metoda sredniej:" << endl;
		for (int i = 0; i < wynikNN.size(); ++i)
		cout << wynikNN[i] << "," << endl;
		//licze pstwa z ciagu uczacego - slabo ze tu, nie mam pomyslu

		pXk1 = bayes.gestoscRownomierny(ciagBadany, min1, max1);
		pXk1 = bayes.gestoscRownomierny(ciagBadany, min2, max2);

		wynikBayes = bayes.metodaBayes(ciagBadany, pXk1, pXk2, bayes.liczPstwo(ilosc1, ilosc1 + ilosc2), bayes.liczPstwo(ilosc2,ilosc1 + ilosc2));
		cout << "pstwo P1: " << bayes.liczPstwo(ilosc1, ilosc1 + ilosc2) << ", pstwo P2: " << bayes.liczPstwo(ilosc1, ilosc1 + ilosc2)<< endl; //potrzebne do badan

		cout <<endl<< "Metoda bayesa:" << endl;
		for (int i = 0; i < wynikBayes.size(); ++i)
		cout << wynikBayes[i] << "," << endl;
		// brak ryzyka :(

		*/
	}

	cout << "srednia: " << srednia/10 << endl;
	int a;
	cin >> a;

	return 0;
}

//http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
//http://www.johndcook.com/blog/2009/01/19/stand-alone-error-function-erf/