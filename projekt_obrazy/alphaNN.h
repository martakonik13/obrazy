#ifndef alphaNN_h
#define alphaNN_h

#include "stdafx.h"
#include <cstdio>
#include <iostream> 
#include <cstdlib>  
#include <cmath>  
#include <ctime>
#include <vector>
#include <algorithm>
#include "Dana.h"

using namespace std;


class alphaNN
{
public:
	static double odleglosc(Dana elemetCiaguUczacego, Dana obj);
	static vector<Dana> sortuj(vector<Dana> &tab);
	static vector<Dana> metodaAlphaNN(vector<Dana> ciagUczacy, vector<Dana> ciagNowy, int alpha);
};

#endif