#include "stdafx.h"
#include "Bayes.h"


using namespace std;


//funkcja bledu - zwiazana z rozkladem normalnym z linku 
double Bayes::erf(double x)
{
	// constants
	double a1 = 0.254829592;
	double a2 = -0.284496736;
	double a3 = 1.421413741;
	double a4 = -1.453152027;
	double a5 = 1.061405429;
	double p = 0.3275911;

	// Save the sign of x
	int sign = 1;
	if (x < 0)
		sign = -1;
	x = abs(x);

	// A&S formula 7.1.26
	double t = 1.0 / (1.0 + p * x);
	double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * exp(-x * x);

	return sign * y;
}
//podaje mu ciag do zakwalifokowania i prawdopodobiensta z funkci nizej (pXk), oraz pstwa apriori pk
//zwraca mi ciag z przyporzadkowanymi klasami
vector<Dana> Bayes::metodaBayes(vector<Dana> ciagNowy, vector<double> pXk1, vector<double> pXk2, double pk1, double pk2)
{
	for (int i = 0; i < ciagNowy.size(); i++)
	{
		double PXK1 = 0, PXK2 = 0;
		// << "1:" << pXk1[i] << ",2:" << pXk2[i] << endl;
		

		PXK1 = (pXk1[i] * pk1);
		PXK2 = (pXk2[i] * pk2);
		cout << "pkx1: " << PXK1 << "  dla: \t" << ciagNowy[i].x << "\t,pkx2: " << PXK2;

		if (PXK1 > PXK2){
			ciagNowy[i].klasa = 1;
			cout << "\t klasa 1"<<endl;
		}
		else{
			ciagNowy[i].klasa = 2;
			cout << "\t klasa 2"<<endl;
		}
	}

	return ciagNowy;
}
//apriori na podstawie tego jakie ciagi generujemy 
double Bayes::liczPrawdopodobienstwo(double iloscklasy, double omega)
{
	return iloscklasy / omega;
}
//gestosc ciagu normalnego - w punktach ciagu do oszacowania 
vector<double> Bayes::gestosc(vector<Dana> ciagNowy, IKlasaRozkladu &klasa)
{
	vector<double> pstwa;
	pstwa.resize(ciagNowy.size());

	for (int i = 0; i < ciagNowy.size(); i++)
	{
		pstwa[i] = klasa.prawdopodobieństwoWystapienia(ciagNowy[i].x);
	}
	cout << endl;
	return pstwa;
}


vector<double> Bayes::liczGranice(IKlasaRozkladu *kl1, IKlasaRozkladu *kl2, double pk1, double pk2){

	vector<double> granice;

	if (KRN *klasa1 = dynamic_cast<KRN*> (kl1)){
		KRN *klasa2 = dynamic_cast<KRN*> (kl2);

		cout << "klasa1 sig: " << (*klasa1).sigma << ",cen: " << (*klasa1).centrum << endl;
		cout << "klasa2 sig: " << (*klasa2).sigma << ",cen: " << (*klasa2).centrum << endl;
		cout << "pk1 " << pk1 << " pk2 " << pk2;

		cout << "Bayes rozklad normalny" << endl;

		double a = pow((*klasa1).sigma, 2) - pow((*klasa2).sigma, 2);
		double b = -2 * (pow((*klasa1).sigma, 2) * (*klasa2).centrum - pow((*klasa2).sigma, 2) * (*klasa1).centrum);
		double c = -2 * log(((1 - pk1) * (*klasa1).sigma) / (pk1 * (*klasa2).sigma)) * pow((*klasa1).sigma, 2) * pow((*klasa2).sigma, 2) + pow((*klasa1).sigma, 2)* pow((*klasa2).centrum, 2) - pow((*klasa2).sigma, 2)* pow((*klasa1).centrum, 2);

		cout << a << ", " << b << ", " << c << " ::: " << endl;
		cout << "granice: " << endl;
		if (a == 0 && b != 0){ // liniowy
			cout << "liniowy: " << -c / b << endl;
			granice.push_back(-c / b);
		}
		else if (a != 0){ //kwadratowy
			double delta = b * b - 4 * a * c;
			if (delta > 0)//dwa pierwiastki
			{
				double x1 = (-b - sqrt(delta)) / (2 * a);
				double x2 = (-b + sqrt(delta)) / (2 * a);
				granice.push_back(x1);
				granice.push_back(x2);
				cout << "x1: " << (min(x1, x2)) << endl;
				cout << "x2: " << (max(x1, x2)) << endl;
			}
			if (delta == 0){//jeden pierwiastek
				cout << "jeden: " << ((-b) / (2 * a)) << endl;
				granice.push_back((-b) / (2 * a));
			}
		}

		cout << "koniec granic" << endl;
		return granice;
	}


	if (KRR *klasa1 = dynamic_cast<KRR*> (kl1)){
		KRR *klasa2 = dynamic_cast<KRR*> (kl2);
		cout << "gdy KRR granice" << endl;

		double min1 = (*klasa1).min;
		double max1 = (*klasa1).max;
		double min2 = (*klasa2).min;
		double max2 = (*klasa2).max;

		// gdy tylko jedna granica
		if (min1 == max2){
			granice.push_back(min1);
			return granice;
		}
		else if (min2 == max1){
			granice.push_back(min2);
			return granice;
		}

		// Jesli min sa takie same wchodzi obojetnie ktore
		if (min1 == min2){
			granice.push_back(min1);
		}
		// Jesli nie sa takie same wchodzi ta ktora jest pomiedzy min max tego drugiego
		else{
			if (min1 > min2 && min1 < max2)
				granice.push_back(min1);
			else if (min2>min1 && min2<max1)
				granice.push_back(min2);
		}


		if (max1 == max2){
			granice.push_back(max1);
		}
		else{
			if (max1>min2 && max1 < max2)
				granice.push_back(max1);
			else if (max2>min1 && max2 < max1)
				granice.push_back(max2);
		}
		cout << "granice ";
		for each (double var in granice)
		{
			cout << var << " ";
		}
		cout << endl;

		return granice;



	}
	return granice;

}



double Bayes::liczRyzyko(IKlasaRozkladu *kl1, IKlasaRozkladu *kl2, vector<double> granice){

	// UWAGA!!
	// Ogarnie tylko jak sa po przeciwnych stronach


	if (KRN *klasa1 = dynamic_cast<KRN*> (kl1)){
		KRN *klasa2 = dynamic_cast<KRN*> (kl2);

		KRN *lewa, *prawa;


		if (granice.size() == 1)
		{
			double x = granice[0];

			if ((*klasa1).centrum < (*klasa2).centrum){
				lewa = klasa1;
				prawa = klasa2;
			}
			else{
				lewa = klasa2;
				prawa = klasa1;
			}

			//double poleZLewejOdGranicy = (1 + erf((var - (*prawa).centrum) / ((*prawa).sigma*sqrt(2)))) / 2;
			double poleZLewejOdGranicy = (*prawa).caukaRozkladu('P', x);
			//double poleZPrawejOdGranicy = 1 - (1 + erf((var - (*lewa).centrum) / ((*lewa).sigma*sqrt(2)))) / 2;
			double poleZPrawejOdGranicy = (*lewa).caukaRozkladu('K', x);

			cout << "blad pole z lewej: " << poleZLewejOdGranicy << " prawej: " << poleZPrawejOdGranicy << endl;
			cout << "granica " << x << " pc " << (*prawa).centrum << " ps " << (*prawa).sigma << endl;
			cout << "BLAD: " << poleZLewejOdGranicy + poleZPrawejOdGranicy << endl;
			return poleZLewejOdGranicy + poleZPrawejOdGranicy;
		}
		if (granice.size() == 2){
			KRN *wewnatrz, *zewnatrz;
			double lewaGranica = min(granice[0], granice[1]);
			double prawaGranica = max(granice[0], granice[1]);

			if (klasa1->sigma > klasa2->sigma){
				wewnatrz = klasa1;
				zewnatrz = klasa2;
			}
			else{
				wewnatrz = klasa2;
				zewnatrz = klasa1;
			}
			double poleWewnatrz = wewnatrz->caukaRozkladu(lewaGranica, prawaGranica);
			double poleZewnatrz = zewnatrz->caukaRozkladu('P', lewaGranica) + zewnatrz->caukaRozkladu('K', prawaGranica);

			return poleWewnatrz + poleZewnatrz;
				cout << "nie umiem liczyc gdy sa 2 granice:(" << endl;
		}
	}
	if (KRR *klasa1 = dynamic_cast<KRR*> (kl1)){
		KRR *klasa2 = dynamic_cast<KRR*> (kl1);

		double ryzyko = 0;
		//costamcos
		cout << "gry KRR 123" << endl;

		cout << " ilosc granic: " << granice.size() << endl;

		if (granice.size() == 2){ // Jesli sa dwie granice to ma sens chyba nie ? gdy jedna to ryzyko 0 ? i gdy 0 granic to nie ma ryzyka
			double wysokosc = min(1 / ((*klasa1).max - (*klasa1).min), 1 / ((*klasa2).max - (*klasa2).min));
			double szerokosc = abs(granice[1] - granice[0]);


			cout << "wysokosc " << wysokosc << " szerokosc " << szerokosc << endl;
			ryzyko = wysokosc * szerokosc;
		}

		return ryzyko; // what is the meaning of life universe and everything else
	}
}