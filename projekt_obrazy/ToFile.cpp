
#include "stdafx.h"
#include "ToFile.h"




void ToFile::zacznij(){
	fstream plik;
	plik.open("mojPlik.txt", ios::out);
	plik.write("%wygenerowane\n\n", 15);
}



void ToFile::wprowadzWektorDoPliku(vector<Dana> vec, string etykieta){

	fstream plik;
	plik.open("mojPlik.txt", ios::app);
	plik.write(etykieta.c_str(), etykieta.length());
	plik << " = [\n";
	for each  (Dana var in vec)
	{
		plik << var.x << ", " << var.klasa << ";\n";
	}
	plik << "]\n" << endl;
}


void ToFile::wprowadzZmiennaDoPliku(double wartosc, string etykieta){

	fstream plik;
	plik.open("mojPlik.txt", ios::app);
	plik.write(etykieta.c_str(), etykieta.length());
	plik << " = " << wartosc << ";\n";
	
}