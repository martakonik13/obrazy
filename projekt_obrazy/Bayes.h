#pragma once

#include "Dana.h"
#include <cmath>

using namespace std;

class Bayes
{
public:
	//prawdopodobienstwo wystapienia danej klasy
	
	static vector<Dana> metodaBayes(vector<Dana> ciagNowy, vector<double> pXk1, vector<double> pXk2, double pk1, double pk2);
	static vector<double> gestosc(vector<Dana> ciagNowy, IKlasaRozkladu &klasa);
	//static vector<double> gestoscRownomierny(vector<Dana> ciagNowy, double min, double max);
	
	static double liczRyzyko(IKlasaRozkladu *kl1, IKlasaRozkladu *kl2, vector<double> granice);
	static double liczPrawdopodobienstwo(double iloscklasy, double omega);
	static double erf(double x);
	static vector<double> liczGranice(IKlasaRozkladu *kl1, IKlasaRozkladu *kl2, double pk1, double pk2);
};

