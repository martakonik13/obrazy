#pragma once

#include "Dana.h"

using namespace std;

class NM
{
public:
	static double srednia(vector<Dana> ciagUczacy, int klasa);
	static double odleglosc(double moda, Dana obj);
	static vector <Dana> metodaNM(vector<Dana> ciagUczacy, vector<Dana> ciagNowy);

};

