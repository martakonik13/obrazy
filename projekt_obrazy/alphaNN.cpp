#include "stdafx.h"
#include <cstdio>
#include <iostream> 
#include <cstdlib>  
#include <cmath>  
#include <vector>
#include "Dana.h"
#include "alphaNN.h"
using namespace std;


double alphaNN::odleglosc(Dana elemCiaguUczacego, Dana obj)
{
	return abs(elemCiaguUczacego.x - obj.x);
}

//babelki! rosnaco
vector<Dana> alphaNN::sortuj(vector<Dana> &tab)
{
	Dana temp;

	sort(tab.begin(), tab.end(), [](Dana a, Dana b){return a.x < b.x; });
	/*
	for (int i = 0; i < tab.size() - 1; i++){
		for (int j = 0; j < tab.size() - 1; j++){
			if (tab[j].x > tab[j + 1].x){
				temp = tab[j + 1];
				tab[j + 1] = tab[j];
				tab[j] = temp;
			}
		}
	}
	*/
	return tab;
}

vector<Dana> alphaNN::metodaAlphaNN(vector<Dana> ciagUczacy, vector<Dana> ciagNowy, int alpha)
{



	for (int z = 0; z < ciagNowy.size(); z++)
	{
		int k1 = 0, k2 = 0; //ilosc sasiadow z danej klasy
		double odlK1 = 0, odlK2 = 0; // odl do najblizszych sasiadow z klas (gdy remis)
		vector<Dana> tab;
		tab.resize(ciagUczacy.size());

		//cout << "id:" << z << endl;
		for (int i = 0; i < ciagUczacy.size(); i++)
		{
			tab[i].x = odleglosc(ciagUczacy[i], ciagNowy[z]);
			tab[i].klasa = ciagUczacy[i].klasa;
		}
		sortuj(tab);
		// wyswietlam posortowane
		//for (int k = 0; k < tab.size(); k++)
//				cout << "x:" << tab[k].x << ",k:" << tab[k].klasa << endl;;
		//	cout << endl;

		for (int j = 0; j < alpha; j++)
		{
			if (tab[j].klasa == 1){ //pierwsza klasa
				k1++;
				odlK1 += tab[j].x;
			}
			else if (tab[j].klasa == 2){ //druga klasa
				k2++;
				odlK2 += tab[j].x;
			}
		}
		//cout << "k1: " <<k1<<","<< odlK1 << "  k2: " <<k2<<","<< odlK2 << endl;
		if (k1 > k2)
			ciagNowy[z].klasa = 1;
		else if (k1 == k2){
			//sprawdz odleglosci 
			if (odlK1 < odlK2)
				ciagNowy[z].klasa = 1;

			else
				ciagNowy[z].klasa = 2;
		}
		else
			ciagNowy[z].klasa = 2;
	}
	return ciagNowy;//ciag nowy z klasami
}